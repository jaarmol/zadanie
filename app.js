var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const { MongoClient } = require("mongodb");

var indexRouter = require('./routes/index.js');
var booksRouter = require('./routes/books');
var movieRouter = require('./routes/movies')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// ---------------------------

//express configuration
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//----------------------------


//routers - add here new routers
app.use('/', indexRouter)
app.use('/books', booksRouter)
// app.post('/books', booksRouter)
// app.put('/books', booksRouter)
app.use('/movies', movieRouter)
// app.post('/movies', movieRouter)
// app.put('/movies', movieRouter)
//----------------------------


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
//----------------------------


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = 'Coś poszło nie tak'; // display error as an message on error page
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
//----------------------------

module.exports = app;
