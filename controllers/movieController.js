const { MongoClient } = require("mongodb");
const newMovie = require('../models/movie')

class MovieController {
  async getAllMovies(req, res) {
    const uri = "mongodb+srv://rhino11:12345admin@cluster1-8pxyd.mongodb.net/test?retryWrites=true&w=majority";
    const client = new MongoClient(uri);
    try {
      await client.connect();
    } catch (e) {
      console.error(e);
    }
    client
      .db("movies")
      .collection("movies")
      .find({})
      .toArray(function(err, converted) {
        if (err) {
          res.send(err);
          client.close();
        }
        if (converted) {
          res.send(converted);
          client.close();
        }
      });
  }

  async getMovie(req, res) {
    const uri =
      "mongodb+srv://rhino11:12345admin@cluster1-8pxyd.mongodb.net/test?retryWrites=true&w=majority";
    const client = new MongoClient(uri);
    try {
      await client.connect();
    } catch (e) {
      console.error(e);
    }
    client
      .db("movies")
      .collection("movies")
      .findOne(nameOfMovie);
    if (err) {
      res.send(err);
      client.close();
    }
    if (converted) {
      res.send(converted);
      client.close();
    }
  }

  async createMovie(req, res, next) {
    const uri =
      "mongodb+srv://rhino11:12345admin@cluster1-8pxyd.mongodb.net/test?retryWrites=true&w=majority";
    const client = new MongoClient(uri);
    try {
      await client.connect();
    } catch (e) {
      console.error(e);
    }
    let movie = new newMovie();
    movie.title = req.body.title,
    movie.author = req.body.author,
    movie.date = req.body.date,
    movie.pages = req.body.pages
    let result = await client
      .db("movies")
      .collection("movies")
      .insertOne(movie);
    res.send(result);
  }
  async updateMovie(req, res) {
    const uri =
      "mongodb+srv://rhino11:12345admin@cluster1-8pxyd.mongodb.net/test?retryWrites=true&w=majority";
    const client = new MongoClient(uri);
    let myQuery = { title: "Shrek"}
    let newValue = { $set: {title: "Toy Story", author: "Luke Skywalker"} }
    try {
      await client.connect();
    } catch (e) {
      console.error(e);
    }
    let result = await client
      .db("movies")
      .collection("movies")
      .updateOne(myQuery, newValue)
      res.send(result); 
  }
}


module.exports = MovieController;
