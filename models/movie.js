class Movie {
    constructor(title, author, date, pages) {
        this.title = title;
        this.author = author;
        this.date = date;
        this.pages = pages;
    }
}

module.exports = Movie;