const BookRepository = require('../repository/bookRepository');

class BookService {

    constructor() {
        this.bookRepository = new BookRepository();
    }

    async createBook(newBook) {
        return await this.bookRepository.createBook(newBook);
    }

    async getAllBooks(uri, client) {
        try {
            await client.connect();
        } catch (err) {
            console.error(e);
        }
        client
            .db("books")
            .collection("books")
            .find()
            .toArray(function (err, converted) {
                if (err) {
                    res.send(err);
                    client.close();
                }
                if (converted) {
                    res.send(converted);
                    client.close();
                }
            });
    }

    async getBook(uri, client) {

        try {
            await client.connect();
        } catch (e) {
            console.error(e);
        }
        client
            .db("books")
            .collection("books")
            .findOne(nameOfBook);
        if (err) {
            res.send(err);
            client.close();
        }
        if (converted) {
            res.send(converted);
            client.close();
        }
    }

    async updateBook(uri, client) {
        let myQuery = { title: req.body.title }
        let newValue = { $set: { title: req.body.title, author: "Luke Skywalker" } }
        try {
            await client.connect();
        } catch (e) {
            console.error(e);
        }
        let result = await client
            .db("books")
            .collection("books")
            .updateOne(myQuery, newValue)
            return result
        // res.send(result);
    }


}



module.exports = BookService


