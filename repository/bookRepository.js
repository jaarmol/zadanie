const DatabaseRepository = require('./databaseRepository');

class BookRepository extends DatabaseRepository{

    async createBook(newBook) {
        await this.openConnection();
        let result = await this.client
            .db("books")
            .collection("books")
            .insertOne(newBook);
        await this.closeConnection();
        return result;
    }

    getAllBooks() {

    }

    getBookByTitle(title) {

    }

    updateBook(book) {

    }

}

module.exports = BookRepository