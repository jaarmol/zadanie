const { MongoClient } = require("mongodb");
const config = require("../config/config.json")


class DatabaseRepository {

  constructor() {
    this.client;
    this.url = config.database_url;
  }

 async openConnection() {
     
    this.client = new MongoClient(this.url);
      try {
          await this.client.connect();
      } catch (e) {
          console.error(e);
      } finally {
  
      }
  }

  async closeConnection() {
    await this.client.close();
  }
  
  async listDatabases(client) {
    databasesList = await this.client.db().admin().listDatabases();
    console.log("Databases:");
    databasesList.databases.forEach(db => console.log(` - ${db.name}`));
  };

}

module.exports = DatabaseRepository;
