var express = require('express');
var router = express.Router();
var MovieController = require('../controllers/movieController');
let newMovie = require('../models/book')
const { MongoClient } = require("mongodb")


const controller = new MovieController();


/* GET users listing. */
router.get('/', controller.getAllMovies.bind(controller));
router.get('/movies', controller.getAllMovies.bind(controller));
router.get('/:id', controller.getMovie.bind(controller));

/* POST users listing. */
router.post('/movies', controller.createMovie.bind(controller));

/* PUT users listing. */
router.put('/movies', controller.updateMovie.bind(controller));

module.exports = router;
