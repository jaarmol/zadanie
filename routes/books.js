var express = require('express');
var router = express.Router();
var BookController = require('../controllers/bookController');
let newBook = require('../models/book')
const { MongoClient } = require("mongodb");
var BookService = require('../service/BookService')



let controller = new BookController();


/* GET users listing. */
router.get('/', controller.getAllBooks.bind(controller));
router.get('/:id', controller.getBook.bind(controller));
/* POST users listing. */
router.post('/', controller.createBook.bind(controller));
/* PUT users listing. */
router.put('/', controller.updateBook.bind(controller));

module.exports = router;



// /* GET users listing. */
// router.get('/', bookService.getAllBooks.bind(controller));
// router.get('/books', bookService.getAllBooks.bind(controller));
// router.get('/:id', bookService.getBook.bind(controller));
// /* POST users listing. */
// router.post('/books', bookService.createBook.bind(controller));
// /* PUT users listing. */
// router.put('/books', bookService.updateBook.bind(controller));

// module.exports = router;
